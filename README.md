octopress-storify
=================

Plugin for including Storify stories in your posts. Since it uses the standard
Storify embed code, it will insert a `script` tag rendering the story, with a 
link to the story in a `noscript` tag also included.

## Installation
Move the `storify.rb` file into the `plugins/` directory of your
Octopress installation.

##  Usage
`{% storify username/name-of-story %}`

### Example
`{% storify zthomae/a-bunch-of-people-who-want-to-know-if-obama-is-gay %}`